// index.js

import { parse } from 'csv-parse/sync';
import { MongoClient } from 'mongodb';

import CsvService from './lib/csv.mjs';
import FileStorageService from './lib/file-storage.mjs';

const csvFiles = [
  { name: 'good-morning-messages', path: './good-morning-messages.csv' },
  { name: 'good-night-messages', path: './good-night-messages.csv' },
  { name: 'birthday-messages', path: './birthday-messages.csv' },
];

const csvService = new CsvService({ parseFn: parse });
const fileStorageService = new FileStorageService();

// iterate through csv files
// read each csv file and parse the content to the csv service
// generate an array using the csv service

const csvOptions = {
  columns: true,
  skip_empty_lines: true,
  quote: '"',
  ignore_last_delimiters: true,
};

let messagesData = {};

for (const csvFile of csvFiles) {
  console.log(`Parsing csv for ${csvFile.name}...`);
  const csvData = await fileStorageService.readFile(csvFile.path);
  messagesData[csvFile.name] = [
    ...(await csvService.parseSync(csvData, csvOptions)).map((entry) => ({
      ...{ number: Number.parseInt(entry.number), content: entry.content },
      createdOn: new Date(),
    })),
  ];
}

console.log('Messages Data: ', messagesData);

function constructOptions() {
  const queryParams = new URLSearchParams('');
  if (
    process.env.MONGODB_SERVER_TLS !== undefined &&
    process.env.MONGODB_SERVER_TLS !== ''
  ) {
    queryParams.set('tls', process.env.MONGODB_SERVER_TLS);
  }
  if (
    process.env.MONGODB_SERVER_TLS_CA_FILE !== undefined &&
    process.env.MONGODB_SERVER_TLS_CA_FILE !== ''
  ) {
    queryParams.set('tlsCAFile', process.env.MONGODB_SERVER_TLS_CA_FILE);
  }
  if (
    process.env.MONGODB_SERVER_RETRY_WRITES !== undefined &&
    process.env.MONGODB_SERVER_RETRY_WRITES !== ''
  ) {
    queryParams.set('retryWrites', process.env.MONGODB_SERVER_RETRY_WRITES);
  }
  return '?' + queryParams.toString();
}

// Connection URI
const uri = `mongodb+srv://${process.env.MONGODB_SERVER_USERNAME}:${
  process.env.MONGODB_SERVER_PASSWORD
}@${process.env.MONGODB_SERVER_HOST}/${constructOptions()}`;

const client = new MongoClient(uri);

async function run(_data) {
  try {
    let result,
      messageNumbers,
      existingMessages,
      existingMessageNumbers,
      newDocuments,
      cursor;
    await client.connect();
    const database = client.db(process.env.MONGODB_SERVER_DATABASE);
    for (const key in _data) {
      console.log('Messages data key: ', key);
      const data = messagesData[key];
      if (key === 'good-morning-messages') {
        const goodMorningMessages = database.collection('GoodMorningMessages');
        const count = await goodMorningMessages.countDocuments();
        if (count === 0) {
          result = await goodMorningMessages.insertMany(data, {
            ordered: true,
          });
          console.log(`Documents inserted: ${result.insertedCount}`);
        } else {
          messageNumbers = data.map((i) => i.number);
          cursor = goodMorningMessages.find({
            number: { $in: messageNumbers },
          });
          existingMessages = await cursor.toArray();
          existingMessageNumbers = existingMessages.map((i) => i.number);
          newDocuments = data.filter(
            (i) => !existingMessageNumbers.includes(i.number)
          );
          if (newDocuments.length !== 0) {
            result = await goodMorningMessages.insertMany(newDocuments, {
              ordered: true,
            });
            console.log(`Documents inserted: ${result.insertedCount}`);
          } else {
            console.log(`${count} documents inserted already`);
          }
        }
      } else if (key === 'good-night-messages') {
        const goodNightMessages = database.collection('GoodNightMessages');
        const count = await goodNightMessages.countDocuments();
        if (count === 0) {
          const result = await goodNightMessages.insertMany(data, {
            ordered: true,
          });
          console.log(`Documents inserted: ${result.insertedCount}`);
        } else {
          messageNumbers = data.map((i) => i.number);
          cursor = goodNightMessages.find({
            number: { $in: messageNumbers },
          });
          existingMessages = await cursor.toArray();
          existingMessageNumbers = existingMessages.map((i) => i.number);
          newDocuments = data.filter(
            (i) => !existingMessageNumbers.includes(i.number)
          );
          if (newDocuments.length !== 0) {
            result = await goodNightMessages.insertMany(newDocuments, {
              ordered: true,
            });
            console.log(`Documents inserted: ${result.insertedCount}`);
          } else {
            console.log(`${count} documents inserted already`);
          }
        }
      } else if (key === 'birthday-messages') {
        const birthdayMessages = database.collection('BirthdayMessages');
        const count = await birthdayMessages.countDocuments();
        if (count === 0) {
          const result = await birthdayMessages.insertMany(data, {
            ordered: true,
          });
          console.log(`Documents inserted: ${result.insertedCount}`);
        } else {
          messageNumbers = data.map((i) => i.number);
          cursor = birthdayMessages.find({
            number: { $in: messageNumbers },
          });
          existingMessages = await cursor.toArray();
          existingMessageNumbers = existingMessages.map((i) => i.number);
          newDocuments = data.filter(
            (i) => !existingMessageNumbers.includes(i.number)
          );
          if (newDocuments.length !== 0) {
            result = await birthdayMessages.insertMany(newDocuments, {
              ordered: true,
            });
            console.log(`Documents inserted: ${result.insertedCount}`);
          } else {
            console.log(`${count} documents inserted already`);
          }
        }
      }
    }
  } finally {
    await client.close();
  }
}

run(messagesData).catch(console.dir);
