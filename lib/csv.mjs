// csv.js

import { DEFAULT_CSV_FILE_SIZE } from '../config.mjs';
import FileStorageService from './file-storage.mjs';

class CsvService extends FileStorageService {
  constructor(options = {}) {
    super(options);
    this._parseFn = options.parseFn;
    this._stringifyFn = options.stringifyFn;
  }

  parseSync(data, options = {}) {
    return this._parseFn(data, options);
  }

  stringifySync(records, options = {}) {
    return this._stringifyFn(records, options);
  }

  isCsvFile(mimeType) {
    return (
      this.getFilesMetadata()[mimeType] &&
      this.getFilesMetadata()[mimeType].mimeType === 'text/csv'
    );
  }

  isCsvFileSizeAllowed(size) {
    return size < DEFAULT_CSV_FILE_SIZE;
  }
}

export default CsvService;
